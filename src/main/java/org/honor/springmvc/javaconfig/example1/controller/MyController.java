package org.honor.springmvc.javaconfig.example1.controller;

import java.util.List;

import org.honor.springmvc.javaconfig.example1.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MyController {
	
	@Autowired
	private GenericService myService;
	
	@RequestMapping("/")
	public String doWelcome(Model model) {
		List<String> welcomeMessage = myService.getWelcomeMessage("friend");
		model.addAttribute("myWelcomeMessage", welcomeMessage);
		return "myView";
	}

}
