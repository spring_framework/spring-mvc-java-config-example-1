package org.honor.springmvc.javaconfig.example1.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class MyService implements GenericService {

	@Override
	public List<String> getWelcomeMessage(String name) {
		List<String> myWelcomeMessage  = new ArrayList<>();
		
		myWelcomeMessage.add("Hello ");
		myWelcomeMessage.add(name);
		myWelcomeMessage.add("! Welcome to 'Configuring a Spring MVC application using Java'...!");
		return myWelcomeMessage;
	}

}
